<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    use HasFactory;

    protected $table = 'ventas';
    protected $primaryKey = 'idVentas';
    protected $fillable = [
        'Clientes_idClientes',
        'Juegos_idJuegos',
        'Sucursal_idSucursal',
    ];

    // Relación con el modelo Cliente
    public function cliente()
    {
        return $this->belongsTo(Cliente::class, 'Clientes_idClientes');
    }

    // Relación con el modelo Juego
    public function juego()
    {
        return $this->belongsTo(Juego::class, 'Juegos_idJuegos');
    }

    // Relación con el modelo Sucursal
    public function sucursal()
    {
        return $this->belongsTo(Sucursal::class, 'Sucursal_idSucursal');
    }
}
