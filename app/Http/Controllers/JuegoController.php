<?php

namespace App\Http\Controllers;

use App\Models\Juego;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use DB;
use Carbon\Carbon;
class JuegoController extends Controller
{
    public function index()
    {
        $juegos = Juego::all();
        return response()->json($juegos);
    }

    public function show($id)
    {
        $juego = Juego::find($id);

        if (!$juego) {
            return response()->json(['message' => 'Juego no encontrado'], 404);
        }

        return response()->json($juego);
    }

    public function store(Request $request)
    {
        $rules= [
            'titulo' => 'required',
            'genero' => 'required',
            'desarrolladora' => 'required',
            'fecha' => 'required',
        ];
        $validator = \Validator::make($request->input(),$rules);
        if ($validator->fails()) {
            return response()->json([
                'status'=>false,
                'errors'=>$validator->errors()->all()
            ], 400);
        }

        Juego::create($request->all());

        return response()->json([
            'status' => true,
            'message' => 'Juego creado correctamente',
            ],200);
    }

    public function update(Request $request, $id)
    {
        $juego = Juego::find($id);

        if (!$juego) {
            return response()->json(['message' => 'Juego no encontrado'], 404);
        }

        $validator = Validator::make($request->all(), [
            'titulo' => 'required',
            'genero' => 'required',
            'desarrolladora' => 'required',
            'fecha' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'=>false,
                'errors'=>$validator->errors()->all()
            ], 400);
        }

        $juego->update($request->all());

        return response()->json([
            'status' => true,
            'message' => 'Juego actualizado',
            ],200);
    }
    public function juegosByVentas(){
        $results = DB::table('ventas')
    ->join('juegos', 'ventas.Juegos_idJuegos', '=', 'juegos.idJuegos')
    ->select('juegos.titulo', DB::raw('COUNT(ventas.Juegos_idJuegos) as total'))
    ->groupBy('juegos.titulo')->get();
        return response()->json($results);
    }

    public function destroy($id)
    {
        try {
            $juego = Juego::findOrFail($id);
            $juego->delete();
            return response()->json(['message' => 'Juego eliminado']);
        } catch (\Illuminate\Database\QueryException $e) {
            // manejar el error, por ejemplo redirigir con un mensaje de error
            return response()->json([
                'status' => false,
                'errors' => 'No se puede eliminar este juego'
            ],404);
        }
    }
}
